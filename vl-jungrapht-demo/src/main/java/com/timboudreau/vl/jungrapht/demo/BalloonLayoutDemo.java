package com.timboudreau.vl.jungrapht.demo;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultGraphType;
import org.jgrapht.graph.builder.GraphBuilder;
import org.jgrapht.graph.builder.GraphTypeBuilder;

import java.io.Serializable;
import java.util.function.Supplier;

/**
 * Demonstrates the visualization of a Tree using TreeLayout and BalloonLayout.
 * An examiner lens performing a hyperbolic transformation of the view is also
 * included.
 *
 * @author Tom Nelson
 */
public class BalloonLayoutDemo {

    private static class IntegerSupplier implements Supplier<Integer>, Serializable {
        private static final long serialVersionUID = -4714266728630636497L;
        private int i = 0;

        public IntegerSupplier(int start) {
            this.i = start;
        }

        public Integer get() {
            return this.i++;
        }
    }

    public static Supplier<Integer> createIntegerSupplier() {
        return new IntegerSupplier(0);
    }

    public static <N, E> Graph<N, E> createTree() {
        GraphBuilder<String, Integer, Graph<String, Integer>> graph =
                GraphTypeBuilder.<String, Integer>forGraphType(DefaultGraphType.dag())
                        .edgeSupplier(createIntegerSupplier())
                        .buildGraphBuilder();

        String base = "Base Node";
        graph.addEdge(base, "B0");
        graph.addEdge(base, "B1");
        graph.addEdge(base, "B2");

        graph.addEdge("B0", "C0");
        graph.addEdge("B0", "C1");
        graph.addEdge("B0", "C2");
        graph.addEdge("B0", "C3");

        graph.addEdge("C2", "H0");
        graph.addEdge("C2", "H1");

        graph.addEdge("B1", "D0");
        graph.addEdge("B1", "D1");
        graph.addEdge("B1", "D2");

        graph.addEdge("B2", "E0");
        graph.addEdge("B2", "E1");
        graph.addEdge("B2", "E2");

        graph.addEdge("D0", "F0");
        graph.addEdge("D0", "F1");
        graph.addEdge("D0", "F2");

        graph.addEdge("D1", "G0");
        graph.addEdge("D1", "G1");
        graph.addEdge("D1", "G2");
        graph.addEdge("D1", "G3");
        graph.addEdge("D1", "G4");
        graph.addEdge("D1", "G5");
        graph.addEdge("D1", "G6");
        graph.addEdge("D1", "G7");

        graph.addEdge(base, "HA1");
        graph.addEdge(base, "HA2");
        graph.addEdge(base, "HA3");
        graph.addEdge("HA3", "I1");
        graph.addEdge("HA3", "I2");

        graph.addEdge("I2", "J1");
        graph.addEdge("K0", "K1");
        graph.addEdge("K0", "K2");
        graph.addEdge("K0", "K3");

        graph.addEdge("J0", "J1");
        graph.addEdge("J0", "J2");
        graph.addEdge("J1", "J4");
        graph.addEdge("J2", "J3");

        graph.addEdge("J2", "J5");
        graph.addEdge("J4", "J6");
        graph.addEdge("J4", "J7");
        graph.addEdge("J3", "J8");
        graph.addEdge("J6", "B9");

        return (Graph<N, E>) graph.build();
    }
}
