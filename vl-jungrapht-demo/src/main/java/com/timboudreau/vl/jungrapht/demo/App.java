/* 
 * Copyright (c) 2013, Tim Boudreau
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.timboudreau.vl.jungrapht.demo;

import com.timboudreau.vl.jungrapht.extensions.BaseJungScene;
import org.jgrapht.Graph;
import org.jungrapht.visualization.VisualizationModel;
import org.jungrapht.visualization.decorators.EdgeShape;
import org.jungrapht.visualization.layout.algorithms.LayoutAlgorithm;
import org.jungrapht.visualization.layout.algorithms.StaticLayoutAlgorithm;
import org.jungrapht.visualization.layout.model.LayoutModel;
import org.jungrapht.visualization.util.Context;
import org.jungrapht.visualization.util.helpers.LayoutFunction;
import org.jungrapht.visualization.util.helpers.LayoutHelper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class App {

    public static void main(String[] args) throws IOException {
        try {
            UIManager.setLookAndFeel(new NimbusLookAndFeel());
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }

        final JFrame jf = new JFrame("Visual Library + JUNGRAPHT Demo");
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Graph gf = BalloonLayoutDemo.createTree();

        Dimension preferredSize = new Dimension(600,600);
        final VisualizationModel<String, Integer> visualizationModel =
                VisualizationModel.builder(gf)
                        .layoutAlgorithm(new StaticLayoutAlgorithm())
                        .layoutSize(preferredSize)
                        .build();

        LayoutModel layoutModel = visualizationModel.getLayoutModel();

        DefaultComboBoxModel<LayoutHelper.Layouts> mdl = new DefaultComboBoxModel<>();
        LayoutHelper.Layouts[] layouts = LayoutHelper.Layouts.values();
        for(LayoutHelper.Layouts l : layouts) {
            mdl.addElement(l);
        }
        mdl.setSelectedItem(LayoutHelper.Layouts.FR);

        // NEW
        LayoutFunction<String> layoutFunction = new LayoutFunction.FullLayoutFunction<>();

        LayoutAlgorithm.Builder<String, ?, ?> builder =
                layoutFunction.apply(LayoutHelper.Layouts.FR.toString());
        LayoutAlgorithm layoutAlgorithm = builder.build();

        final BaseJungScene scene = new SceneImpl(gf, layoutAlgorithm, layoutModel);
        jf.setLayout(new BorderLayout());
        jf.add(new JScrollPane(scene.createView()), BorderLayout.CENTER);

        JToolBar bar = new JToolBar();
        bar.setMargin(new Insets(5, 5, 5, 5));
        bar.setLayout(new FlowLayout(5));

        final JCheckBox checkbox = new JCheckBox("Animate iterative layouts");

        scene.setLayoutAnimationFramesPerSecond(48);

        final JComboBox<LayoutHelper.Layouts> cmbLayouts = new JComboBox(mdl);
        cmbLayouts.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

                if (value instanceof LayoutHelper.Layouts)
                {
                    LayoutHelper.Layouts layoutObject = (LayoutHelper.Layouts)value;
                    setText( layoutObject.toString());
                }

                return this;
            }
        });
        bar.add(new JLabel(" Layout Type"));
        bar.add(cmbLayouts);

        cmbLayouts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                LayoutHelper.Layouts layout = (LayoutHelper.Layouts) cmbLayouts.getSelectedItem();

                // NEW
                LayoutAlgorithm.Builder<String, ?, ?> builder =
                        layoutFunction.apply(layout.toString());
                LayoutAlgorithm layoutAlgorithm = builder.build();

                scene.setGraphLayout(layoutAlgorithm, true);
            }
        });

        bar.add(new JLabel(" Connection Shape"));
        DefaultComboBoxModel<Function<Context<Graph<String, String>, String>, Shape>> shapes = new DefaultComboBoxModel<>();
        shapes.addElement(EdgeShape.quadCurve());
        shapes.addElement(EdgeShape.cubicCurve());
        shapes.addElement(EdgeShape.line());
        shapes.addElement(EdgeShape.orthogonal());

        final JComboBox<Function<Context<Graph<String, String>, String>, Shape>> shapesBox = new JComboBox<>(shapes);
        shapesBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                Function<Context<Graph<String, String>, String>, Shape> xform = (Function<Context<Graph<String, String>, String>, Shape>) shapesBox.getSelectedItem();
                scene.setConnectionEdgeShape(xform);
            }
        });
        shapesBox.setRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                String label = "";
                if(value instanceof EdgeShape.QuadCurve) {
                    label = "Quad Curve";
                }
                else if(value instanceof EdgeShape.CubicCurve) {
                    label = "Cubic Curve";
                }
                else if(value instanceof EdgeShape.Line) {
                    label = "Line";
                }
                else if(value instanceof EdgeShape.Orthogonal) {
                    label = "Orthogonal";
                }
                setText(label);
                return this;
            }
        });
        shapesBox.setSelectedItem(new EdgeShape.QuadCurve<>());
        bar.add(shapesBox);
        jf.add(bar, BorderLayout.NORTH);
        bar.add(new MinSizePanel(scene.createSatelliteView()));
        bar.setFloatable(false);
        bar.setRollover(true);

        final JLabel selectionLabel = new JLabel("<html>&nbsp;</html>");
        Lookup.Result<String> selectedNodes = scene.getLookup().lookupResult(String.class);
        selectedNodes.allInstances();
        LookupListener listener = new LookupListener() {
            @Override
            public void resultChanged(LookupEvent le) {
                Lookup.Result<String> res = (Lookup.Result<String>) le.getSource();
                StringBuilder sb = new StringBuilder("<html>");
                List<String> l = new ArrayList<>(res.allInstances());
                Collections.sort(l);
                for (String s : l) {
                    if (sb.length() != 6) {
                        sb.append(", ");
                    }
                    sb.append(s);
                }
                sb.append("</html>");
                selectionLabel.setText(sb.toString());
            }
        };
        selectionLabel.putClientProperty("ll", listener); // ensure it's not garbage collected
        selectionLabel.putClientProperty("lr", selectedNodes); // ensure it's not garbage collected
        selectedNodes.addLookupListener(listener);
        selectedNodes.allInstances();


        checkbox.setSelected(true);
        checkbox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                scene.setAnimateIterativeLayouts(checkbox.isSelected());
            }
        });
        bar.add(checkbox);
        bar.add(selectionLabel);
        selectionLabel.setHorizontalAlignment(SwingConstants.TRAILING);

//        jf.setSize(jf.getGraphicsConfiguration().getBounds().width - 120, 700);
        jf.setSize(new Dimension(1280, 720));
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jf.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent we) {
                scene.relayout(true);
                scene.validate();
            }
        });
        jf.setVisible(true);
    }

    private static class MinSizePanel extends JPanel {

        MinSizePanel(JComponent inner) {
            setLayout(new BorderLayout());
            add(inner, BorderLayout.CENTER);
            setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        }

        public Dimension getPreferredSize() {
            Dimension result = super.getPreferredSize();
            result.height = 40;
            return result;
        }
    }
}
