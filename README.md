JUNGRAPHT + NetBeans' Visual Library Integration
===========================================

This project aims to port the great work by Tim Boudreau on his VL-JUNG library (http://github.com/timboudreau/vl-jung) from JUNG to JUNGRAPHT.

The goal is to leverage as much as possible of Tim's work and make minimal changes to adopt the use of JungraphtT.
